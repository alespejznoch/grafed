Two Haskell projects made in my first year at university. (2008)
The [HGL](https://hackage.haskell.org/package/HGL) is used for the graphic output, but it is now abandoned, which makes compilation of this project difficult.

**editor.hs** 
* example of simple vector graphic editor
* it allows working with basic graphic objects
* loading and storing of picture files is also possible

**rendering.hs**
* program for rendering of picture files from editor.hs
* also allows export to Targa (*.tga) format
