import System.IO
import Graphics.HGL hiding (char, line)
import System.Directory
import Text.ParserCombinators.Parsec
import Char
import Data.Maybe




------------------------------------------------------------------------------------------------
------------------------------- PARSOVANI ------------------------------------------------
identFunkce :: Parser String -- ctverec, kruh, mnohouhelnik..
identFunkce = do 
		c <- many (letter <|> digit <|> space  <|> oneOf "?>.<,;:']}[{)(_-")
		return c
       <?> "identifikator Funkce"

identHodnoty :: Parser Int -- pozice bodu, polomery..
identHodnoty = do 
		c <- many1 digit
		return (read c)
       <?> "identifikator Hodnoty"
       
identBarvy :: Parser String -- Black, White..
identBarvy = do 
		c <- many letter
		return c
       <?> "identifikator Barvy"

komentar :: Parser () -- pro komentare uvnitr souboru *.ge
komentar = do
		char '#'
		skipMany (noneOf "\r\n")
        <?> "komentar"


polozka :: Parser (String, Int, Int, Int, Int, Int, Int, String) -- jedna polozka v seznamu, sestavena pomoci parseru
polozka = do
		funkce	<-	identFunkce
		skipMany space
		char '='
		skipMany space
		hodnotaA	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaB	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaC	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaD	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaE	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaF	 <-	 identHodnoty
		char ','
		skipMany space
		barva		 <-	 identBarvy
		return (funkce, hodnotaA, hodnotaB, hodnotaC, hodnotaD, hodnotaE, hodnotaF, barva)



radek :: Parser (Maybe (String, Int, Int, Int, Int, Int, Int, String))
radek = do
		skipMany space
		try (komentar >> return Nothing) <|> (polozka >>= return . Just)

vse :: Parser [(String, Int, Int, Int, Int, Int, Int, String)] -- vysledny parser
vse = do
		radky <- many radek
		return (catMaybes radky)


ctiObsah :: String -> IO () -- ulozi obsah souboru *.ge do seznamu a preda ho dal funkci 'go'
ctiObsah soubor  = do
		result <- parseFromFile vse soubor
		case (result) of
                	Left err  -> print err
			Right xs  -> go soubor xs




----------------------------------------------------------------------------------------------------------------------------
--------------------------------- ZOBRAZENI ---------------------------------------------------------------------------
go :: String -> [(String, Int, Int, Int, Int, Int, Int, String)] -> IO ()  -- spusti graficke okno
go soubor  xs = runGraphics $ do
		w <- openWindowEx "Grafed - pro zavreni grafickeho okna zmackni klavesu" (Just (10,10)) (320,200) DoubleBuffered (Just 1)
		smyckaGo xs w
		drawInWindow w $ (withColor Green)  (text (200, 180) "zmackni klavesu")
		getKey w
		closeWindow w

smyckaGo :: [(String,Int, Int, Int, Int, Int, Int, String)] -> Window -> IO ()  -- zobrazovani grafickych objektu podle seznamu
smyckaGo ((funkce, hodnotaA, hodnotaB, hodnotaC, hodnotaD, hodnotaE, hodnotaF, barva):xs) w = do
		case funkce of
		
			"ctverec"		-> do
							drawInWindow w $ withColor (read barva) (polyline [(hodnotaA, hodnotaB),(hodnotaC, hodnotaB),(hodnotaC, hodnotaD),(hodnotaA, hodnotaD),(hodnotaA, hodnotaB)])
			"kruh"		-> do
							drawInWindow w $ withColor (read barva) (ellipse (hodnotaA-hodnotaC, hodnotaB-hodnotaC) (hodnotaA+hodnotaC,hodnotaB+hodnotaC))
			"kruznice"		-> do
							drawInWindow w $ withColor (read barva) (arc (hodnotaA-hodnotaC, hodnotaB-hodnotaC) (hodnotaA+hodnotaC,hodnotaB+hodnotaC) 0 360)
			"elipsa"		-> do
							drawInWindow w $ withColor (read barva) (arc (hodnotaA-hodnotaC, hodnotaB-hodnotaD) (hodnotaA+hodnotaC,hodnotaB+hodnotaD) 0 360)
			"plnaelipsa"	-> do
							drawInWindow w $ withColor (read barva) (ellipse (hodnotaA-hodnotaC, hodnotaB-hodnotaD) (hodnotaA+hodnotaC,hodnotaB+hodnotaD))
			"linka"		-> do
							drawInWindow w $ withColor (read barva) (polyline [(hodnotaA, hodnotaB),(hodnotaC,hodnotaD)])
			"polygon"		-> do
							drawInWindow w $ withColor (read barva) (polygon [(hodnotaA, hodnotaB),(hodnotaC,hodnotaD),(hodnotaE,hodnotaF),(hodnotaA,hodnotaB)])
			"test"		-> do
							drawInWindow w $ (ellipse (hodnotaA-hodnotaC, hodnotaB-hodnotaC) (hodnotaA+hodnotaC,hodnotaB+hodnotaC))
							
			_			-> do
							drawInWindow w $ withColor (read barva) (text (hodnotaA, hodnotaB) funkce)
			_		-> do putStrLn "hotovo"
		
		smyckaGo xs w
smyckaGo [] w = putStrLn "hotovo"


---------------------------------------------------------------------------------------------------------------
------------------------------- EDITACE --------------------------------------------------------------------
obsluha	:: String -> IO () -- prijimani prikazu z uzivatelskeho rozhrani
obsluha	 soubor = do
	putStr "editace > "
	prikaz <- getLine
	case prikaz of
			"konec"		->	do
								ctiObsah soubor
								
			"zobraz"		->	do
								ctiObsah soubor
								
			"zpet"		->	do
								zpet soubor
								
			"pomoc"		->	do
								putStrLn "prikazy: \nzobraz, zpet, konec\nctverec, kruh, kruznice, elipsa, plnaelipsa, lomenacara, mnohouhelnik"
								
	
	------------------------------------------------------------------------------------------------------------------------------------------ CTVEREC ------------------------------------------------------------------------------------------------
			"ctverec"		->	do
								putStr "Zadej  X  souradnici leveho horniho rohu > "
								hodnotaA <- getLine
								if isInt hodnotaA then do
													putStr "Zadej  Y  souradnici leveho horniho rohu > "
													hodnotaB <- getLine
													if isInt hodnotaB then do
																		putStr "Zadej  X  souradnici praveho dolniho rohu > "
																		hodnotaC <- getLine
																		if isInt hodnotaC then do
																							putStr "Zadej  Y  souradnici praveho dolniho rohu > "
																							hodnotaD <- getLine
																							if isInt hodnotaD then do
																												putStr "Zadej  barvu ctverce ( Black, Blue, Green, Cyan, Red, Magenta, Yellow, White) > "
																												barva <- getLine
																												if isColor barva then do
																													appendFile soubor ("\n" ++ "ctverec=" ++ hodnotaA ++ "," ++ hodnotaB ++ "," ++
																														hodnotaC ++ "," ++ hodnotaD ++ ",0,0," ++ barva)
																														
																															else putStrLn "Chybne zadani !!!"
																																
																										else putStrLn "Chybne zadani !!!"
																										
																					else putStrLn "Chybne zadani !!!"
																						
																else putStrLn "Chybne zadani !!!"
																	
											else putStrLn "Chybne zadani !!!"
											
	
	---------------------------------------------------------------------------------------------------------------------------------------------- KRUH ------------------------------------------------------------------------------------------------------
			"kruh"		->	do
								putStr "Zadej  X  souradnici stredu > "
								hodnotaA <- getLine
								if isInt hodnotaA then do
													putStr "Zadej  Y  souradnici stredu > "
													hodnotaB <- getLine
													if isInt hodnotaB then do
																		putStr "Zadej  polom�r > "
																		hodnotaC <- getLine
																		if isInt hodnotaC then do
																							putStr "Zadej  barvu kruhu ( Black, Blue, Green, Cyan, Red, Magenta, Yellow, White) > "
																							barva <- getLine
																							if isColor barva then do
																								appendFile soubor ("\n" ++ "kruh=" ++ hodnotaA ++ "," ++ hodnotaB ++ "," ++
																									hodnotaC ++ ",0,0,0," ++ barva)
																									
																										else putStrLn "Chybne zadani !!!"
																																
																									
																										
																					else putStrLn "Chybne zadani !!!"
																						
																else putStrLn "Chybne zadani !!!"
																	
											else putStrLn "Chybne zadani !!!"
								
											
																			
	
	---------------------------------------------------------------------------------------------------------------------------------------------- KRUZNICE ---------------------------------------------------------------------------------------------------
			"kruznice"		->	do
								putStr "Zadej  X  souradnici stredu > "
								hodnotaA <- getLine
								if isInt hodnotaA then do
													putStr "Zadej  Y  souradnici stredu > "
													hodnotaB <- getLine
													if isInt hodnotaB then do
																		putStr "Zadej  polom�r > "
																		hodnotaC <- getLine
																		if isInt hodnotaC then do
																							putStr "Zadej  barvu kruznice ( Black, Blue, Green, Cyan, Red, Magenta, Yellow, White) > "
																							barva <- getLine
																							if isColor barva then do
																											appendFile soubor ("\n" ++ "kruznice=" ++ hodnotaA ++ "," ++ hodnotaB ++ "," ++
																													hodnotaC ++ ",0,0,0," ++ barva)
																													
																										else putStrLn "Chybne zadani !!!"
																																
																							
																									
																										
																					else putStrLn "Chybne zadani !!!"
																						
																else putStrLn "Chybne zadani !!!"
																	
											else putStrLn "Chybne zadani !!!"
								
								
											
	
	------------------------------------------------------------------------------------------------------------------------------------------------------ ELIPSA -----------------------------------------------------------------------------------
			"elipsa"		->	do
								putStr "Zadej  X  souradnici stredu > "
								hodnotaA <- getLine
								if isInt hodnotaA then do
													putStr "Zadej  Y  souradnici stredu > "
													hodnotaB <- getLine
													if isInt hodnotaB then do
																		putStr "Zadej  velikost hlavni osy > "
																		hodnotaC <- getLine
																		if isInt hodnotaC then do
																							putStr "Zadej  velikost vedlejsi osy > "
																							hodnotaD <- getLine
																							if isInt hodnotaD then do
																												putStr "Zadej  barvu elipsy ( Black, Blue, Green, Cyan, Red, Magenta, Yellow, White) > "
																												barva <- getLine
																												if isColor barva then do
																													appendFile soubor ("\n" ++ "elipsa=" ++ hodnotaA ++ "," ++ hodnotaB ++ "," ++
																														hodnotaC ++ "," ++ hodnotaD ++ ",0,0," ++ barva)
																														
																															else putStrLn "Chybne zadani !!!"
																																
																										else putStrLn "Chybne zadani !!!"
																									
																										
																					else putStrLn "Chybne zadani !!!"
																						
																else putStrLn "Chybne zadani !!!"
																	
											else putStrLn "Chybne zadani !!!"
								
	
	--------------------------------------------------------------------------------------------------------------------------------------------------- PLNAELIPSA --------------------------------------------------------------------------------
			"plnaelipsa"		->	do
								putStr "Zadej  X  souradnici stredu > "
								hodnotaA <- getLine
								if isInt hodnotaA then do
													putStr "Zadej  Y  souradnici stredu > "
													hodnotaB <- getLine
													if isInt hodnotaB then do
																		putStr "Zadej  velikost hlavni osy > "
																		hodnotaC <- getLine
																		if isInt hodnotaC then do
																							putStr "Zadej  velikost vedlejsi osy > "
																							hodnotaD <- getLine
																							if isInt hodnotaD then do
																												putStr "Zadej  barvu plne elipsy ( Black, Blue, Green, Cyan, Red, Magenta, Yellow, White) > "
																												barva <- getLine
																												if isColor barva then do
																													appendFile soubor ("\n" ++ "plnaelipsa=" ++ hodnotaA ++ "," ++ hodnotaB ++ "," ++
																														hodnotaC ++ "," ++ hodnotaD ++ ",0,0," ++ barva)
																														
																															else putStrLn "Chybne zadani !!!"
																																
																										else putStrLn "Chybne zadani !!!"
																									
																										
																					else putStrLn "Chybne zadani !!!"
																						
																else putStrLn "Chybne zadani !!!"
																	
											else putStrLn "Chybne zadani !!!"
								
	
			"lomenacara"		->	do
									lomenaCara soubor
								
			"mnohouhelnik"	->	do
									mnohouhelnik soubor
							
			_			->	do
								putStrLn "Chybne zadani !!! napis pomoc pro zobrazeni moznosti"
	
	
	if prikaz/="konec" then	do
						obsah <- readFile soubor
						putStrLn ("============ OBSAH SOUBORU ==============\n" ++ obsah ++ "\n\n============ KONEC OBSAHU =============\n")
						obsluha soubor
					else main

					
					
					
lomenaCara :: String -> IO ()
lomenaCara soubor = do
							putStr "Zadej  X  souradnici bodu A > "
							hodnotaA <- getLine
							if isInt hodnotaA then do
												putStr "Zadej  Y  souradnici bodu A > "
												hodnotaB <- getLine
												if isInt hodnotaB then do
																	cLomenaCara soubor hodnotaA hodnotaB "nic"
																					
															else putStrLn "Chybne zadani !!!"
																	
										else putStrLn "Chybne zadani !!!"
								
cLomenaCara :: String -> String -> String -> String-> IO ()
cLomenaCara soubor a b barva = do
						putStr "Zadej  X  souradnici dalsiho bodu > "
						hodnotaC <- getLine
						if isInt hodnotaC then do
											putStr "Zadej  Y  souradnici dalsiho bodu > "
											hodnotaD <- getLine
											if isInt hodnotaD then do
															if barva=="nic" then do
																putStr "Zadej  barvu lomene cary ( Black, Blue, Green, Cyan, Red, Magenta, Yellow, White) > "
																barva <- getLine
																if isColor barva then do
																				appendFile soubor ("\n" ++ "linka=" ++ a ++ "," ++ b ++ "," ++
																					hodnotaC ++ "," ++ hodnotaD ++ ",0,0," ++ barva)
																				
																				putStr "Dalsi bod? ( A nebo N ) > "
																				dalsi <- getLine
																				if dalsi=="A" then cLomenaCara soubor hodnotaC hodnotaD barva else putStr "f"
																														
																			else putStrLn "Chybne zadani !!!"
																
																		else do
																			appendFile soubor ("\n" ++ "linka=" ++ a ++ "," ++ b ++ "," ++
																					hodnotaC ++ "," ++ hodnotaD ++ ",0,0," ++ barva)
																				
																			putStr "Dalsi bod? ( A nebo N ) > "
																			dalsi <- getLine
																			if dalsi=="A" then cLomenaCara soubor hodnotaC hodnotaD barva else putStr "f"
																														
																							
														else putStrLn "Chybne zadani !!!"
																									
																										
									else putStrLn "Chybne zadani !!!"

mnohouhelnik :: String -> IO ()
mnohouhelnik soubor = do
							putStr "Zadej  X  souradnici bodu A > "
							hodnotaA <- getLine
							if isInt hodnotaA then do
												putStr "Zadej  Y  souradnici bodu A > "
												hodnotaB <- getLine
												if isInt hodnotaB then do
																	putStr "Zadej  X  souradnici bodu B > "
																	hodnotaC <- getLine
																	if isInt hodnotaC then do
																						putStr "Zadej  Y  souradnici bodu B > "
																						hodnotaD <- getLine
																						if isInt hodnotaD then do
																											cmnohouhelnik soubor hodnotaA hodnotaB hodnotaC hodnotaD "nic"
																									else putStrLn "Chybne zadani !!!"
																				else putStrLn "Chybne zadani !!!"
																				 
															else putStrLn "Chybne zadani !!!"
																	
										else putStrLn "Chybne zadani !!!"
								
cmnohouhelnik :: String -> String -> String -> String -> String -> String -> IO ()
cmnohouhelnik soubor a b c d barva = do
						putStr "Zadej  X  souradnici dalsiho bodu > "
						hodnotaE <- getLine
						if isInt hodnotaE then do
											putStr "Zadej  Y  souradnici dalsiho bodu > "
											hodnotaF <- getLine
											if isInt hodnotaF then do
																if barva=="nic" then do
																				putStr "Zadej  barvu mnoho�helniku ( Black, Blue, Green, Cyan, Red, Magenta, Yellow, White) > "
																				barva <- getLine
																				if isColor barva then do
																								appendFile soubor ("\n" ++ "polygon=" ++ a ++ "," ++ b ++ "," ++
																									c ++ "," ++ d ++ "," ++ hodnotaE ++ "," ++ hodnotaF ++ "," ++ barva)
																								putStr "Dalsi bod? ( A nebo N ) > "
																								dalsi <- getLine
																								if dalsi=="A" then cmnohouhelnik soubor a b hodnotaE hodnotaF barva else putStr "f"
																							
																							else putStrLn "Chybne zadani !!!"
																								
																			else do 
																				appendFile soubor ("\n" ++ "polygon=" ++ a ++ "," ++ b ++ "," ++
																					c ++ "," ++ d ++ "," ++ hodnotaE ++ "," ++ hodnotaF ++ "," ++ barva)
																				putStr "Dalsi bod? ( A nebo N ) > "
																				dalsi <- getLine
																				if dalsi=="A" then cmnohouhelnik soubor a b hodnotaE hodnotaF barva else putStr "f"
																								
																							
														else putStrLn "Chybne zadani !!!"
																									
																										
									else putStrLn "Chybne zadani !!!"


zpet :: String -> IO () -- ulozi soubor do seznamu a preda ho dal funkci smazPosledni
zpet soubor = do
		result <- parseFromFile vse soubor
		case (result) of
                	Left err  -> print err
			Right xs  -> smazPosledni soubor xs
			
smazPosledni :: String -> [(String, Int, Int, Int, Int, Int, Int, String)] -> IO () -- vymaze obsah souboru a pak spusti funkci mazaniPosledni
smazPosledni soubor xs = do
				writeFile soubor ""
				mazaniPosledni soubor (take ((length xs)-1) xs)

mazaniPosledni :: String -> [(String, Int, Int, Int, Int, Int, Int, String)] -> IO () -- polozky seznamu ulozi zpet do souboru
mazaniPosledni soubor ((fce,a,b,c,d,e,f,x):xs) = do
								appendFile soubor ("\n" ++ fce ++ "=" ++ (show a) ++ "," ++ (show b) ++ "," ++ (show c) ++ "," ++ (show d) ++ "," ++ (show e) ++ "," ++ (show f) ++ "," ++ x)
								mazaniPosledni soubor xs
mazaniPosledni soubor [] = putStrLn "Krok zp�t proveden"



isColor :: String -> Bool
isColor barva = if ( (barva=="Black") || (barva=="Blue") || (barva=="Green") || (barva=="Cyan") || (barva=="Red") || (barva=="Magenta") || (barva=="Yellow") || (barva=="White")) then True else False
isColor "" = False
		
isInt :: String -> Bool
isInt (x:xs) = if isDigit x then if (length xs)==0 then True else isInt xs else False
isInt "" = False



--------------------------------------------------------------------------------------------------------------------------
------------------------- PRACE SE SOUBORY, ZACATEK -----------------------------------------------------
uvod = putStrLn "novy   - novy obrazek \nnacti  - nacte obrazek\n"

obsluhaChyby _ = return "Chyba pri nacitani souboru"

main :: IO ()
main =  do
	putStrLn "\n"
	putStrLn "       OOO      OOO      OO     OOOOO   OOOOO    OO   "
	putStrLn "      O        O   O    O  O    O       O        O O  "
	putStrLn "      O OO     O  O     O  O    OOO     OOO      O  O "
	putStrLn "      O  O     OOO      OOOO    O       O        O O  "
	putStrLn "      OOO      O  OO    O  O    O       OOOOO    OO   "
	putStrLn ""
	putStrLn "\n\n\n       Vitejte v programu Grafed - jednoduch� grafick� vektorov� editor."	
	putStrLn "\n=================================================================\n"
	putStrLn "    napiste \"pomoc\" pro zobrazeni aktualniho seznamu pouziteln�ch prikaz�\n"

	
novy :: IO ()
novy = do
			putStrLn "Napis jmeno souboru ( *.ge )"
			soubor <- getLine
			existuje <- doesFileExist soubor
			if existuje then do
				putStrLn ("Soubor " ++ show (soubor) ++ " jiz existuje. Zadejte jin� nazev souboru")
				novy
			else do 
				obsah <- (catch (nacteni soubor) obsluhaChyby)
				if (obsah)/="" then	
						putStrLn (show(obsah))
				else do
						putStrLn ("Soubor "++ show (soubor) ++ " byl �sp�sn� vytvoren")
						putStrLn "napis pomoc pro seznam prikaz�"
						obsluha soubor





nacti :: IO ()
nacti = do
			putStrLn "Napis jmeno souboru ( *.ge )"
			soubor <- getLine
			existuje <- doesFileExist soubor
			if existuje then do
				obsah <- (catch (nacteni soubor) obsluhaChyby)
				putStrLn ("Soubor "++ show (soubor) ++ " byl �sp�sn� nacten")
				putStrLn "napis pomoc pro seznam prikaz�"
				obsluha soubor
				
				
			else do 
				putStrLn ("Soubor " ++ show (soubor) ++ " neexistuje. Zadejte jin� nazev souboru")
				nacti


nacteni :: String -> IO String
nacteni soubor = do
			handle <- openFile soubor ReadWriteMode
		 	hGetContents handle
