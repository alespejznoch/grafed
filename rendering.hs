import System.IO
import Text.ParserCombinators.Parsec
import Char
import Data.Maybe
import Data.Word
import qualified Data.ByteString as B





------------------------------------------------------------------------------------------------
------------------------------- PARSOVANI ------------------------------------------------
identFunkce :: Parser String -- ctverec, kruh, mnohouhelnik..
identFunkce = do 
		c <- many (letter <|> digit <|> space  <|> oneOf "?>.<,;:']}[{)(_-")
		return c
       <?> "identifikator Funkce"

identHodnoty :: Parser Int -- pozice bodu, polomery..
identHodnoty = do 
		c <- many1 digit
		return (read c)
       <?> "identifikator Hodnoty"
       
identBarvy :: Parser String -- Black, White..
identBarvy = do 
		c <- many letter
		return c
       <?> "identifikator Barvy"

komentar :: Parser () -- pro komentare uvnitr souboru *.ge
komentar = do
		char '#'
		skipMany (noneOf "\r\n")
        <?> "komentar"


polozka :: Parser (String, Int, Int, Int, Int, Int, Int, String) -- jedna polozka v seznamu, sestavena pomoci parseru
polozka = do
		funkce	<-	identFunkce
		skipMany space
		char '='
		skipMany space
		hodnotaA	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaB	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaC	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaD	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaE	 <-	 identHodnoty
		char ','
		skipMany space
		hodnotaF	 <-	 identHodnoty
		char ','
		skipMany space
		barva		 <-	 identBarvy
		return (funkce, hodnotaA, hodnotaB, hodnotaC, hodnotaD, hodnotaE, hodnotaF, barva)



radek :: Parser (Maybe (String, Int, Int, Int, Int, Int, Int, String))
radek = do
		skipMany space
		try (komentar >> return Nothing) <|> (polozka >>= return . Just)

vse :: Parser [(String, Int, Int, Int, Int, Int, Int, String)] -- vysledny parser
vse = do
		radky <- many radek
		return (catMaybes radky)


ctiBin :: String -> IO [Int]
ctiBin soubor  = do
		g <- readFile soubor
		return (charToInts g)

ctiObsah :: String -> IO ()
ctiObsah soubor  = do
		result <- parseFromFile vse soubor
		case (result) of
                	Left err  -> print err
			Right xs  -> smyckaExport  xs
-------------------------------------------------------------------------------------------------
--------------------- PRAZDNY TGA SOUBOR -----------------------------------------
hlavicka :: [Word8]
hlavicka = [0,1,1,0,0,0,1,24,0,0,0,0,64,1,200,0,8,0]

paleta :: [Word8]  -- BGR  Black, Blue, Green, Cyan, Red, Magenta, Yellow, White .. + prazdno
paleta = [0,0,0,  255,0,0,  0,255,0,  255,255,0,  0,0,255,  255,0,255,  0,255,255,  255,255,255]

bordel :: [Word8]
bordel = [a |a <- [0], n <- [1..744]]

pixely :: [Word8]
pixely = [a |a <- [1], n <- [1..64000]]

-------------------------------------------------------------------------------------------------
--------------------- POMOCNE FUNKCE PRO PREVODZY A JINE -----------------
zaokrouhleni :: Int -> Int -> Int
zaokrouhleni x y = if ((x `mod` y)>=(y `div` 2)) then ((x `div` y) +1)
					else ( x `div` y)

			
charToInt :: Char -> Int
charToInt znak = case znak of
						'0' -> 0
						'1' -> 1
						'2' -> 2
						'3' -> 3
						'4' -> 4
						'5' -> 5
						'6' -> 6
						'7' -> 7
						'8' -> 8

						
charToInts :: [Char] -> [Int]
charToInts (x:xs) = (charToInt x) : (charToInts xs)
charToInts _ = []

intToChar :: Int -> Char
intToChar cislice = case cislice of
						0 -> '0'
						1 -> '1'
						2 -> '2'
						3 -> '3'
						4 -> '4'
						5 -> '5'
						6 -> '6'
						7 -> '7'
						8 -> '8'

						
intsToChar :: [Int] -> [Char]
intsToChar (x:xs) = (intToChar x) : (intsToChar xs)
intsToChar _ = []

			
word8ToString :: Word8 -> Char
word8ToString binarni = case binarni of
						0 -> '0'
						1 -> '1'
						2 -> '2'
						3 -> '3'
						4 -> '4'
						5 -> '5'
						6 -> '6'
						7 -> '7'
						8 -> '8'
						49 -> '1'



word8sToString :: [Word8] -> [Char]
word8sToString (x:xs) = (word8ToString x) : (word8sToString xs)
word8sToString _ = []


charToWord8 :: Char -> Word8
charToWord8 binarni = case binarni of
						'0' -> 0
						'1' -> 1
						'2' -> 2
						'3' -> 3
						'4' -> 4
						'5' -> 5
						'6' -> 6
						'7' -> 7
						'8' -> 8



stringToWord8s :: [Char] -> [Word8]
stringToWord8s (x:xs) = (charToWord8 x) : (stringToWord8s xs)
stringToWord8s _ = []

						
intToDouble :: Int -> Double 
intToDouble n = fromInteger (toInteger n) 


cisloBarvy :: String -> Int
cisloBarvy barva = case barva of
			"Black" -> 0
			"Blue" -> 1
			"Green" -> 2
			"Cyan" -> 3
			"Red" -> 4
			"Magenta" -> 5
			"Yellow" -> 6
			"White" -> 7
-------------------------------------------------------------------------------------------------
--------------------- EXPORT ---------------------------------------------------------------
export :: String -> String -> IO ()
export soubor2 soubor = do
			handle <- openBinaryFile soubor ReadWriteMode	
			writeFile "cache.dat" (  (word8sToString ( pixely)))	
			ctiObsah soubor2
			g <- readFile "cache.dat"		
			B.writeFile soubor (B.pack (hlavicka ++ paleta ++ bordel ++ ( stringToWord8s g) ))
			hClose handle
			putStr "fd"
			
			
			
smyckaExport :: [(String,Int, Int, Int, Int, Int, Int, String)] -> IO ()
smyckaExport ((funkce, hodnotaA, hodnotaB, hodnotaC, hodnotaD, hodnotaE, hodnotaF, barva):xs) = do

	
		case funkce of
		
			"ctverec"		-> do
							ctverec hodnotaA hodnotaB  hodnotaC hodnotaD (cisloBarvy barva)
			"kruh"		-> do
							kruh hodnotaA hodnotaB  hodnotaC (cisloBarvy barva)
			"kruznice"		-> do
							kruznice hodnotaA hodnotaB  hodnotaC (cisloBarvy barva)
			"elipsa"		-> do
							elipsa hodnotaA hodnotaB  hodnotaD hodnotaC (cisloBarvy barva)
			"plnaelipsa"	-> do
							plnaElipsa hodnotaA hodnotaB  hodnotaD hodnotaC (cisloBarvy barva)
			"linka"		-> do
							linka hodnotaA hodnotaB  hodnotaC hodnotaD (cisloBarvy barva)							
			"polygon"		-> do
							polygon hodnotaA hodnotaB  hodnotaC hodnotaD hodnotaE hodnotaF (cisloBarvy barva)
									
			_		-> do putStrLn "hotovo"
		

		smyckaExport xs	
smyckaExport [] = putStrLn "hotovo"


---------------------------------------------------------------------------------------------------
------------------------------------- vkladani do seznamu ----------------------------------
vloz :: Int -> Int-> IO [Int] -> IO [Int]  -- zmeni barvu jednoho pixelu
vloz n x s = do
			seznam <- s
			return ( (take (n-1) seznam) ++ [x] ++ (drop (n) seznam) )
			
vem :: Int -> IO [Int] -> IO Int  --zjisti barvu jednoho pixelu
vem n s = do
			seznam <- s
			return ( head (drop (n-1) seznam ) )

		
{-			
vlozT :: Int -> Int-> IO [Int] -> IO [Int]
vlozT n x s = do
				seznam <- s
				return ( (take (n-2-320) seznam) ++ [x,x,x] ++ (take (320-3) (drop (n-320+1) seznam)) ++ [x,x,x] ++ (take (320-3) (drop (n+1) seznam)) ++ [x,x,x] ++ (drop (n+320-1+2) seznam) )
-}
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------- LINKA --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
linka :: Int -> Int -> Int -> Int -> Int -> IO ()
linka x1 y1 x2 y2 barva  = do
				
					if ((smernice x1 y1 x2 y2) >1) || ((smernice x1 y1 x2 y2) < (-1)) then do
					   if y1<y2 then do
						f <- smyckaLinkaY (intToDouble x1) y1 y2 (1/(smernice x1 y1 x2 y2)) barva (ctiBin "cache.dat")
						print (drop 64002 f)
						writeFile "cache.dat"  ( intsToChar f )
						
					   else do
						f <- smyckaLinkaY (intToDouble x2) y2 y1 (1/(smernice x2 y2 x1 y1)) barva (ctiBin "cache.dat")
						print (drop 64002 f)
						writeFile "cache.dat"  ( intsToChar f )
											
					else do
					    if x1<x2 then do
						f <- smyckaLinkaX x1 (intToDouble y1) x2 (smernice x1 y1 x2 y2) barva (ctiBin "cache.dat")
						print (drop 64002 f)
						writeFile "cache.dat"  ( intsToChar f )
						
					    else do
						f <- smyckaLinkaX x2 (intToDouble y2) x1 (smernice x2 y2 x1 y1) barva (ctiBin "cache.dat")
						print (drop 64002 f)
						writeFile "cache.dat"  ( intsToChar f )
						
						
						
				

smyckaLinkaX :: Int -> Double -> Int -> Double -> Int -> IO [Int] -> IO [Int]
smyckaLinkaX x1 y1 x2 smernice barva seznam = do
					if (x1<=x2) then do
								smyckaLinkaX (x1+1) ( y1 + (smernice) ) x2 smernice barva (vloz (320*( round (200-y1))+x1) barva seznam)
							else do
								seznam
								
smyckaLinkaY :: Double -> Int -> Int -> Double -> Int -> IO [Int] -> IO [Int]
smyckaLinkaY x1 y1 y2 smernice barva seznam = do
					if (y1<=y2) then do
								smyckaLinkaY (x1+(smernice)) ( y1+1) y2 smernice barva (vloz (320*(200-y1)+( round x1)) barva seznam)
							else do
								seznam
								
smernice :: Int -> Int -> Int -> Int -> Double
smernice x1 y1 x2 y2 = (intToDouble(y2 - y1)) / (intToDouble (x2 - x1))
					
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------- CTVEREC --------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ctverec :: Int -> Int -> Int -> Int -> Int -> IO ()
ctverec x1 y1 x2 y2 barva = do
			linka x1 y1 x2 y1 barva
			linka x2 y1 x2 y2 barva
			linka x2 y2 x1 y2 barva
			linka x1 y2 x1 y1 barva
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------- KRUZNICE --------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
kruznice :: Int -> Int -> Int -> Int -> IO ()
kruznice x y r barva = do
		f <- smyckaKruznice 0 r (1-r) 3 ((2*r)-2) barva (ctiBin "cache.dat") x y
		print (drop 64002 f)
		writeFile "cache.dat"  ( intsToChar f )


smyckaKruznice :: Int -> Int -> Int -> Int -> Int -> Int -> IO [Int] -> Int -> Int -> IO [Int]
smyckaKruznice x y p dvex dvey barva seznam pozx pozy= do
								if x<=y then do
										if p>0 then smyckaKruznice (x+1) (y-1) (p-dvey+dvex) (dvex + 2) (dvey -2)   barva   (vloz (320*(200-pozy-y)+(pozx+x)) barva (vloz (320*(200-pozy+y)+(pozx+x)) barva 	(vloz (320*(200-pozy-y)+(pozx-x)) barva (vloz (320*(200-pozy+y)+(pozx-x)) barva (vloz (320*(200-pozy-x)+(pozx+y)) barva (vloz (320*(200-pozy+x)+(pozx+y)) barva (vloz (320*(200-pozy-x)+(pozx-y)) barva (vloz (320*(200-pozy+x)+(pozx-y)) barva seznam) ))))))) pozx pozy
										
										
										
											else smyckaKruznice (x+1) y (p+dvex) (dvex + 2) dvey  barva  (vloz (320*(200-pozy-y)+(pozx+x)) barva (vloz (320*(200-pozy+y)+(pozx+x)) barva 	(vloz (320*(200-pozy-y)+(pozx-x)) barva (vloz (320*(200-pozy+y)+(pozx-x)) barva (vloz (320*(200-pozy-x)+(pozx+y)) barva (vloz (320*(200-pozy+x)+(pozx+y)) barva (vloz (320*(200-pozy-x)+(pozx-y)) barva (vloz (320*(200-pozy+x)+(pozx-y)) barva seznam) ))))))) pozx pozy
									else seznam
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------- KRUZNICET --------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
{-
kruzniceT :: Int -> Int -> Int -> IO ()
kruzniceT x y r = do
		f <- smyckaKruzniceT 0 r (1-r) 3 ((2*r)-2) (ctiBin "cache.dat") x y
		print (drop 64002 f)
		writeFile "cache.dat"  ( intsToChar f )


smyckaKruzniceT :: Int -> Int -> Int -> Int -> Int -> IO [Int] -> Int -> Int -> IO [Int]
smyckaKruzniceT x y p dvex dvey seznam pozx pozy= do
								if x<=y then do
										if p>0 then smyckaKruzniceT (x+1) (y-1) (p-dvey+dvex) (dvex + 2) (dvey -2)      (vlozT (320*(pozy+y)+(pozx+x)) 6 (vlozT (320*(pozy-y)+(pozx+x)) 6 	(vlozT (320*(pozy+y)+(pozx-x)) 6 (vlozT (320*(pozy-y)+(pozx-x)) 6 (vlozT (320*(pozy+x)+(pozx+y)) 6 (vlozT (320*(pozy-x)+(pozx+y)) 6 (vlozT (320*(pozy+x)+(pozx-y)) 6 (vlozT (320*(pozy-x)+(pozx-y)) 6 seznam) ))))))) pozx pozy
										
										
										
											else smyckaKruzniceT (x+1) y (p+dvex) (dvex + 2) dvey    (vlozT (320*(pozy+y)+(pozx+x)) 6 (vlozT (320*(pozy-y)+(pozx+x)) 6 	(vlozT (320*(pozy+y)+(pozx-x)) 6 (vlozT (320*(pozy-y)+(pozx-x)) 6 (vlozT (320*(pozy+x)+(pozx+y)) 6 (vlozT (320*(pozy-x)+(pozx+y)) 6 (vlozT (320*(pozy+x)+(pozx-y)) 6 (vlozT (320*(pozy-x)+(pozx-y)) 6 seznam) ))))))) pozx pozy
									else seznam
-}
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------- KRUH --------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
kruh :: Int -> Int -> Int -> Int -> IO ()
kruh x y r barva = do
			kruznice x y r 8
			seminkoR (x,y) barva
			kruznice x y r barva

{-
kruh :: Int -> Int -> Int -> IO ()
kruh x y r = do
			kruznice x y r
			cyklusKruh x y (r-2)
			


cyklusKruh :: Int -> Int -> Int -> IO ()
cyklusKruh x y 1 = kruzniceT x y 1
cyklusKruh x y 0 = kruzniceT x y 1
cyklusKruh x y (-1) = kruzniceT x y 1
cyklusKruh x y r = do
			kruzniceT x y (r)
			cyklusKruh x y (r-3)
-}
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------- ELIPSA -------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
elipsa :: Int -> Int -> Int -> Int -> Int -> IO ()
elipsa x y a b barva = elipsaX (intToDouble x) (intToDouble y) (intToDouble a) (intToDouble b) barva


elipsaX :: Double -> Double -> Double -> Double -> Int -> IO ()
elipsaX x y a b barva = do
		f <- smyckaElipsa2 0 b a b ( (b^2)-b*(a^2)+(a^2) / 4  ) barva (ctiBin "cache.dat")  x y
		print (drop 64002 f)
		writeFile "cache.dat"  ( intsToChar f )
									
smyckaElipsa2 :: Double -> Double -> Double -> Double -> Double -> Int -> IO [Int] -> Double -> Double -> IO [Int]
smyckaElipsa2  x y a b d barva seznam pozx pozy= do
								if ( (a^2)*(y-0.5) >= (b^2)*(x+1) ) then do
										if d<0 then smyckaElipsa2 (x+1) (y) a b ( d+(b^2)*(2*x+3) )  barva  (vloz (( round (320*(200-pozy-x )+(pozx+y)))  ) barva (vloz (( round (320*(200-pozy+x )+(pozx+y)))  ) barva (vloz (( round (320*(200-pozy+x )+(pozx-y)))  ) barva (vloz (( round (320*(200-pozy-x )+(pozx-y)))  ) barva seznam)))) pozx pozy
										
										
										
											else smyckaElipsa2 (x+1) (y-1) a b ( d+(b^2)*(2*x+3)+(a^2)*((-2)*y+2) )   barva (vloz ((round (320*(200-pozy-x)+(pozx+y)) )) barva (vloz (( round (320*(200-pozy+x )+(pozx+y)))  ) barva (vloz (( round (320*(200-pozy+x )+(pozx-y)))  ) barva (vloz (( round (320*(200-pozy-x )+(pozx-y)))  ) barva seznam)))) pozx pozy
									else smyckaElipsa3 x y a b ( (b^2)*(x+0.5)*(x+0.5)+(a^2)*(y-1)*(y-1)-(a^2)*(b^2)  ) barva seznam pozx pozy	
										
										
smyckaElipsa3 :: Double -> Double -> Double -> Double -> Double -> Int -> IO [Int] -> Double -> Double -> IO [Int]
smyckaElipsa3  x y a b d barva seznam pozx pozy = do
								if ( y>=0 ) then do
										if d<0 then smyckaElipsa3 (x+1) (y-1) a b ( d+(b^2)*(2*x+2)+(a^2)*((-2)*y+3 ))   barva  (vloz (( round (320*(200-pozy-x )+(pozx+y)))  ) barva (vloz (( round (320*(200-pozy+x )+(pozx+y)))  ) barva (vloz (( round (320*(200-pozy+x )+(pozx-y)))  ) barva (vloz (( round (320*(200-pozy-x )+(pozx-y)))  ) barva seznam)))) pozx pozy
										
										
										
											else smyckaElipsa3 (x) (y-1) a b ( d+(a^2)*((-2)*y+3) )  barva (vloz (( round (320*(200-pozy-x )+(pozx+y)))  ) barva (vloz (( round (320*(200-pozy+x )+(pozx+y)))  ) barva (vloz (( round (320*(200-pozy+x )+(pozx-y)))  ) barva (vloz (( round (320*(200-pozy-x )+(pozx-y)))  ) barva seznam)))) pozx pozy
									else seznam
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------- ELIPSAT ------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
{-
elipsaT :: Double -> Double -> Double -> Double -> IO ()
elipsaT x y a b = do
		f <- smyckaElipsa2T 0 b a b ( (b^2)-b*(a^2)+(a^2) / 4  ) (ctiBin "cache.dat")  x y
		print (drop 64002 f)
		writeFile "cache.dat"  ( intsToChar f )
									
smyckaElipsa2T :: Double -> Double -> Double -> Double -> Double -> IO [Int] -> Double -> Double -> IO [Int]
smyckaElipsa2T  x y a b d seznam pozx pozy= do
								if ( (a^2)*(y-0.5) >= (b^2)*(x+1) ) then do
										if d<0 then smyckaElipsa2T (x+1) (y) a b ( d+(b^2)*(2*x+3) )    (vlozT (( round (320*(pozy+x )+(pozx+y)))  ) 6 (vlozT (( round (320*(pozy-x )+(pozx+y)))  ) 6 (vlozT (( round (320*(pozy-x )+(pozx-y)))  ) 6 (vlozT (( round (320*(pozy+x )+(pozx-y)))  ) 6 seznam)))) pozx pozy
										
										
										
											else smyckaElipsa2T (x+1) (y-1) a b ( d+(b^2)*(2*x+3)+(a^2)*((-2)*y+2) )    (vlozT ((round (320*(pozy+x)+(pozx+y)) )) 6 (vlozT (( round (320*(pozy-x )+(pozx+y)))  ) 6 (vlozT (( round (320*(pozy-x )+(pozx-y)))  ) 6 (vlozT (( round (320*(pozy+x )+(pozx-y)))  ) 6 seznam)))) pozx pozy
									else smyckaElipsa3T x y a b ( (b^2)*(x+0.5)*(x+0.5)+(a^2)*(y-1)*(y-1)-(a^2)*(b^2)  ) seznam pozx pozy	
										
										
smyckaElipsa3T :: Double -> Double -> Double -> Double -> Double -> IO [Int] -> Double -> Double -> IO [Int]
sSmyckaElipsa3T  x y a b d seznam pozx pozy = do
								if ( y>=0 ) then do
										if d<0 then smyckaElipsa3T (x+1) (y-1) a b ( d+(b^2)*(2*x+2)+(a^2)*((-2)*y+3 ))     (vlozT (( round (320*(pozy+x )+(pozx+y)))  ) 6 (vlozT (( round (320*(pozy-x )+(pozx+y)))  ) 6 (vlozT (( round (320*(pozy-x )+(pozx-y)))  ) 6 (vlozT (( round (320*(pozy+x )+(pozx-y)))  ) 6 seznam)))) pozx pozy
										
										
										
											else smyckaElipsa3T (x) (y-1) a b ( d+(a^2)*((-2)*y+3) )   (vlozT (( round (320*(pozy+x )+(pozx+y)))  ) 6 (vlozT (( round (320*(pozy-x )+(pozx+y)))  ) 6 (vlozT (( round (320*(pozy-x )+(pozx-y)))  ) 6 (vlozT (( round (320*(pozy+x )+(pozx-y)))  ) 6 seznam)))) pozx pozy
									else seznam
-}
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------- PLNAELIPSA -----------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
plnaElipsa :: Int -> Int -> Int -> Int -> Int -> IO ()
plnaElipsa x y a b barva = do
			elipsa x y a b 8
			seminkoR (x,y) barva
			elipsa x y a b barva

{-
plnaElipsa :: Double -> Double -> Double -> Double -> IO ()
plnaElipsa x y a b = do
			elipsa x y a b
			cyklusPlnaElipsa x y (a-2) (b-2)
			


cyklusPlnaElipsa :: Double -> Double -> Double -> Double -> IO ()
cyklusPlnaElipsa x y 1 b = eElipsaT x y 1 b
cyklusPlnaElipsa x y 0 b = eElipsaT x y 1 b
cyklusPlnaElipsa x y (-1) b = eElipsaT x y 1 b
cyklusPlnaElipsa x y a 1  = eElipsaT x y a 1
cyklusPlnaElipsa x y a 0 = eElipsaT x y a 1
cyklusPlnaElipsa x y a (-1) = eElipsaT x y a 1
cyklusPlnaElipsa x y a b = do
			elipsaT x y (a) (b)
			cyklusPlnaElipsa x y (a-3) (b-3) 
-}
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------- POLYGON -----------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


polygon :: Int -> Int -> Int -> Int -> Int -> Int -> Int -> IO ()
polygon x1 y1 x2 y2 x3 y3 barva = do
			linka x1 y1 x2 y2 8
			linka x2 y2 x3 y3 8
			linka x3 y3 x1 y1 8
			
			seminko (  stred (x1,y1) ( stred (x2, y2) (x3,y3)) ) barva
			
			linka x1 y1 x2 y2 barva
			linka x2 y2 x3 y3 barva
			linka x3 y3 x1 y1 barva

							  
stred :: (Int,Int) -> (Int,Int) -> (Int,Int)
stred (x1,y1) (x2,y2) = let 
					      a1=intToDouble x1
					      b1=intToDouble y1
					      a2=intToDouble y2
					      b2=intToDouble y2
					      
					      in if a1==a2 then if b1==b2 then (  round a1 , round b1  )
							                         else  if b1<b2 then (  round a1 , round (((b2-b1) / 2)+b1)  )
							  			                        else ( round a1 , round (((b2-b1) / 2)+b1)  )
						        else if a1<a2 then if b1==b2 then  (  round (((a2-a1) / 2)+a1) , round b1  )
								       					     else  if b1<b2 then (  round (((a2-a1) / 2)+a1) ,  round (((b2-b1) / 2)+b1)  )
													        		    else ( round (((a2-a1) / 2)+a1) , round (((b1-b2) / 2)+b2)  )
																    
										   else if b1==b2 then  ( round (((a1-a2) / 2)+a2) , round b1  )
										   				else  if b1<b2 then ( round (((a1-a2) / 2)+a2) , round (((b2-b1) / 2)+b1)  )
																       else (  round (((a1-a2) / 2)+a2) ,  round (((b2-b1) / 2)+b1)  )

			

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------- VYPLNOVANI POMOCI SEMINKOVE METODY -----------------------------------------------------------------------------------------------------------------------------
seminko :: (Int,Int) -> Int -> IO ()  -- seminkova metoda
seminko (x,y) barva = do
	f <- (vem (320*(200-y)+x) (ctiBin "cache.dat" ) )
	if (f/=barva) && (f/=8) then do
		g <- vloz (320*(200-y)+x) barva (ctiBin "cache.dat")
		print (drop 64002 g)
		writeFile "cache.dat"  ( intsToChar g )
		
		
		seminko (x+1,y) barva
		
		seminko (x-1,y) barva
		
		seminko (x,y+1) barva
		
		seminko (x,y-1) barva
		
		
	   else putStr "X"
	   
	   

seminkoR :: (Int,Int) -> Int -> IO ()  -- upravena seminkova metoda, ktera zaplnuje prostor po jednotlivych radcich
seminkoR (x,y) barva = do
		eT (x,y+1) barva
		eD (x,y) barva

		
	   
eT :: (Int,Int) -> Int -> IO ()
eT (x,y) barva = do
		g <- eL (x-1,y) barva (eR (x,y) barva (ctiBin "cache.dat"))
		print (drop 64002 g)
		writeFile "cache.dat"  ( intsToChar g )
		
		f <- (vem (320*((200-y)-1)+x) (ctiBin "cache.dat") )
		if (f/=8) then do
			eT (x,y+1) barva
		  else putStr ""


eD :: (Int,Int) -> Int -> IO ()
eD (x,y) barva = do				
		g <- eL (x-1,y) barva (eR (x,y) barva (ctiBin "cache.dat"))
		print (drop 64002 g)
		writeFile "cache.dat"  ( intsToChar g )
		
		f <- (vem (320*((200-y)+1)+x) (ctiBin "cache.dat") )
		if (f/=8) then do
			eD (x,y-1) barva
		  else putStr ""
	   
eL :: (Int,Int) -> Int -> IO [Int] -> IO [Int]
eL (x,y) barva s = do
			f <- (vem (320*(200-y)+x) s)
			if (f/=8) then do
				
				eL (x-1,y) barva (vloz (320*(200-y)+x) barva s)
			  else s
	   
eR :: (Int,Int) -> Int -> IO [Int] -> IO [Int]
eR (x,y) barva s = do
			f <- (vem (320*(200-y)+x) s)
			if (f/=8) then do
			
				eR (x+1,y) barva (vloz (320*(200-y)+x) barva s)
			  else s
			  
			  



main :: IO ()
main =  do
	putStrLn "\n"
	putStrLn "       OOO      OOO      OO     OOOOO   OOOOO    OO   "
	putStrLn "      O        O   O    O  O    O       O        O O  "
	putStrLn "      O OO     O  O     O  O    OOO     OOO      O  O "
	putStrLn "      O  O     OOO      OOOO    O       O        O O  "
	putStrLn "      OOO      O  OO    O  O    O       OOOOO    OO   "
	putStrLn ""
	putStrLn "\n\n\n       Vitejte v programu Grafed Render - jednoduchy prevadec vektoroveho obrazku do rastroveho."	
	putStrLn "\n=================================================================\n"
	putStrLn "    napiste export \"soubor.ge\" \"soubor.tga\" pro prevedeni obrazku soubor.ge do souboru soubor.tga\n"
